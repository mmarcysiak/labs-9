/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author st
 */
public class KonsultacjeFabryka implements ConsultationListFactory, Serializable{
    
    @Override
    public ConsultationList create() {
        ConsultationList consultationList = new ListKonsultacji();
       return consultationList;
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList consultationListList = new ListKonsultacji();
        //list = new ListKonsultacji();
        
        if(deserialize){
        try{
            XMLDecoder dekoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("a.xml")));
            consultationListList =(ConsultationList) dekoder.readObject();
            dekoder.close();
        }
        catch(FileNotFoundException e) {
            Logger.getLogger(KonsultacjeFabryka.class.getName()).log(Level.SEVERE,null,e);
        }   
        return consultationListList;
        }        
        else return create();
    }

    @Override
    public void save(ConsultationList consultationList) {
        try{
            XMLEncoder enkoder = new XMLEncoder(new BufferedOutputStream( new FileOutputStream("a.xml")));
            enkoder.writeObject(consultationList.getConsultation());
            enkoder.close();
        }
        catch(FileNotFoundException e){
            System.out.print(e.getMessage());
        }
      }
    
    
}
