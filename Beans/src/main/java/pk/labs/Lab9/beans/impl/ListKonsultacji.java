/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import java.util.*;



/**
 *
 * @author st
 */
public class ListKonsultacji implements ConsultationList, Serializable{
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private Consultation[] list ;
    
    public ListKonsultacji(){
        list = new Consultation[]{};
    }
    
    @Override
    public int getSize() {
        return list.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.list;
    }

    public void setConsultation(Consultation[] list){
        list=list;
    }
    @Override
    public Consultation getConsultation(int index) {
        return this.list[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        
        Consultation[] nowaListaConsultations = Arrays.copyOf(this.list, this.list.length + 1);
        Consultation[] staraListaConsultations = this.list;
        nowaListaConsultations[nowaListaConsultations.length - 1] = consultation;        
        this.list = nowaListaConsultations;       
        propertyChangeSupport.firePropertyChange("consultation", staraListaConsultations, nowaListaConsultations);
       
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
}
