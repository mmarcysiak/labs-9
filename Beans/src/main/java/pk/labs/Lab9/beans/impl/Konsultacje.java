/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;

/**
 *
 * @author st
 */
public class Konsultacje implements Consultation {
    
     private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
     private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);
    String student;
    Term termin;
    
    public Konsultacje(){
        
    }
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student=student;
    }

    @Override
    public Date getBeginDate() {
        return this.termin.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.termin.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term staryTermin = this.termin;
        vetoableChangeSupport.fireVetoableChange("Termin", staryTermin, term);
        this.termin = term;
        propertyChangeSupport.firePropertyChange("Termin", staryTermin, term);
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        
 if(minutes <= 0) minutes=0;  
        int czasTrwania = this.termin.getDuration();
        this.vetoableChangeSupport.fireVetoableChange("Termin", czasTrwania, czasTrwania + minutes);
        this.termin.setDuration(this.termin.getDuration() + minutes);
        this.propertyChangeSupport.firePropertyChange("Termin", czasTrwania, czasTrwania + minutes);    
    }
}
