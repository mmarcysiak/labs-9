/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Termin implements Term, Serializable{

    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    int duration;
    Date begin;
    
    public Termin(){
        
    }
    @Override
    public Date getBegin() {

        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin=begin;
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            this.duration=duration;
        }

    }

    @Override
    public Date getEnd() {
        return new Date(this.duration * 1000*60 + this.begin.getTime() );
    }
    
}
