package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.Konsultacje;
import pk.labs.Lab9.beans.impl.KonsultacjeFabryka;
import pk.labs.Lab9.beans.impl.ListKonsultacji;
import pk.labs.Lab9.beans.impl.Termin;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Termin.class;
    public static Class<? extends Consultation> consultationBean = Konsultacje.class;
    public static Class<? extends ConsultationList> consultationListBean = ListKonsultacji.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = KonsultacjeFabryka.class;
    
}
